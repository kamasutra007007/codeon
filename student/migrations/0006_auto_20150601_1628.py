# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0005_modelschanges'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentgroup',
            name='headman',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, blank=True, to='student.Student', verbose_name='Headman'),
        ),
    ]
