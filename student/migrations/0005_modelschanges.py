# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0004_auto_20150530_1859'),
    ]

    operations = [
        migrations.CreateModel(
            name='ModelsChanges',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('model_name', models.CharField(max_length=100, verbose_name='Model name')),
                ('obj_id', models.IntegerField(verbose_name=b'Object ID')),
                ('operation', models.IntegerField(default=0, choices=[(0, b'CREATION'), (1, b'EDITING'), (2, b'REMOVAL')])),
                ('operation_date', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
