# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0003_auto_20150530_0130'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='ticket',
            field=models.CharField(unique=True, max_length=6, verbose_name='Ticket number'),
        ),
    ]
