from exceptions import RuntimeError

from django import template
from django.core import urlresolvers
from django.contrib.auth import get_user_model

from student.models import Student, StudentGroup

User = get_user_model()

register = template.Library()


@register.simple_tag
def edit_url(obj):
    if isinstance(obj, Student):
        return urlresolvers.reverse('admin:student_studentgroup_change', args=(obj.group.pk,))
    elif isinstance(obj, StudentGroup):
        return urlresolvers.reverse('admin:student_studentgroup_change', args=(obj.pk,))
    elif isinstance(obj, User):
        return urlresolvers.reverse('admin:auth_user_change', args=(obj.pk,))
    RuntimeError("Unknown object type")
