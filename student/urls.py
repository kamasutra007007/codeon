from django.conf.urls import patterns, url

from student.views import StudentGroupList, StudentGroupDelete, StudentGroupAdd, StudentGroupUpdate, StudentsOfGroup, \
    StudentUpdate, StudentCreate, StudentDelete


urlpatterns = patterns('',
    url(r'^all/$', StudentGroupList.as_view(), name='group_list'),
    url(r'^delete/(?P<pk>[\d]+)/$', StudentGroupDelete.as_view(), name='group_delete'),
    url(r'^add/$', StudentGroupAdd.as_view(), name='group_add'),
    url(r'^update/(?P<pk>[\d]+)/$', StudentGroupUpdate.as_view(), name='group_update'),
    url(r'^students/(?P<group_id>[\d]+)/$', StudentsOfGroup.as_view(), name='students_of_group'),

    url(r'^students/update/(?P<pk>[\d]+)/$', StudentUpdate.as_view(), name='student_update'),
    url(r'^students/add/$', StudentCreate.as_view(), name='student_add'),
    url(r'^students/delete/(?P<pk>[\d]+)/$', StudentDelete.as_view(), name='student_delete'),
)
