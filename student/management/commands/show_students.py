# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.management.base import BaseCommand
from student.models import StudentGroup


class Command(BaseCommand):
    def handle(self, *args, **options):
        for group in StudentGroup.objects.all():
            print '='*50
            print 'Group {} with {} student(s)'.format(group.name, group.students_of_group.count())
            print '='*50
            for student in group.students_of_group.all():
                print '{} {} {} {} {}'.format(student.birthday, student.ticket,
                                                student.last_name, student.first_name, student.middle_name)
        print '='*50
