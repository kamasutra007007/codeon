from django.core.urlresolvers import reverse_lazy
from django.http.response import HttpResponseRedirect
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.db.models import Count

from student.models import StudentGroup, Student


class LoginRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return login_required(view)


# Groups
class StudentGroupList(LoginRequiredMixin, ListView):
    # model = StudentGroup
    queryset = StudentGroup.objects.all().select_related("headman").annotate(students_count=Count("students_of_group"))
    template_name = 'student/groups_show.html'


class StudentGroupDelete(LoginRequiredMixin, DeleteView):
    model = StudentGroup
    template_name = 'student/group_delete.html'
    success_url = reverse_lazy('student:group_list')


class StudentGroupAdd(LoginRequiredMixin, CreateView):
    model = StudentGroup
    template_name = 'student/group_add.html'
    fields = ['name']
    success_url = reverse_lazy('student:group_list')


class StudentGroupUpdate(LoginRequiredMixin, UpdateView):
    model = StudentGroup
    template_name = 'student/group_update.html'
    fields = ['name', 'headman']
    success_url = reverse_lazy('student:group_list')

    def get_context_data(self, **kwargs):
        context = super(StudentGroupUpdate, self).get_context_data(**kwargs)
        context['form'].fields['headman'].queryset = self.object.students_of_group.all()
        return context


class StudentsOfGroup(LoginRequiredMixin, ListView):
    model = Student
    template_name = 'student/group_members.html'

    def get_queryset(self):
        self.group = get_object_or_404(StudentGroup, id=self.kwargs['group_id'])
        return self.group.students_of_group.all()

    def get_context_data(self, **kwargs):
        context = super(StudentsOfGroup, self).get_context_data(**kwargs)
        context['group'] = self.group
        context['headman'] = self.group.headman
        return context


# Students
class StudentDelete(LoginRequiredMixin, DeleteView):
    model = Student
    template_name = 'student/student_delete.html'
    success_url = reverse_lazy('student:group_list')


class StudentUpdate(LoginRequiredMixin, UpdateView):
    model = Student
    template_name = 'student/student_update.html'
    fields = ['first_name', 'last_name', 'middle_name', 'photo', 'birthday', 'ticket', 'group']

    def form_valid(self, form):
        self.object = form.save()

        # What if we change headman of group?
        if self.object.group.pk != form.initial['group']:
            try:
                group = StudentGroup.objects.get(id=form.initial['group'])
                if group.headman == self.object:
                    group.headman = None
                    group.save()
            except StudentGroup.DoesNotExist, error:
                print error
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy('student:students_of_group', kwargs={'group_id': self.object.group.pk})


class StudentCreate(LoginRequiredMixin, CreateView):
    model = Student
    template_name = 'student/student_add.html'
    fields = ['first_name', 'last_name', 'middle_name', 'photo', 'birthday', 'ticket', 'group']

    def get_success_url(self):
        return reverse_lazy('student:students_of_group', kwargs={'group_id': self.object.group.pk})
