import os
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext_lazy as _

OPERATION_CHOICES = (
    (0, 'CREATION'),
    (1, 'EDITING'),
    (2, 'REMOVAL'),
)


class StudentGroup(models.Model):
    name = models.CharField(max_length=30, verbose_name=_('Name'))
    headman = models.OneToOneField('student.Student', null=True, blank=True,
        verbose_name=_('Headman'), on_delete=models.SET_NULL)

    def __unicode__(self):
        return self.name

    def clean(self):
        if self.headman and self.headman.group.pk != self.pk:
                raise ValidationError(_('Student must be a member of group.'))


class Student(models.Model):
    first_name = models.CharField(max_length=30, verbose_name=_('First name'))
    last_name = models.CharField(max_length=30, verbose_name=_('Last name'))
    middle_name = models.CharField(max_length=30, verbose_name=_('Middle name'))
    photo = models.ImageField(upload_to='students_photo', verbose_name=_('Photo'))
    birthday = models.DateField(verbose_name=_('Birthday'))
    ticket = models.CharField(max_length=6, unique=True, verbose_name=_('Ticket number'))
    group = models.ForeignKey(StudentGroup, related_name='students_of_group', verbose_name=_('Group'))

    def __unicode__(self):
        return self.ticket


# Drop image before delete Student object
def drop_image(sender, instance, **kwargs):
    try:
        if instance.photo and os.path.isfile(instance.photo.path):
            os.remove(instance.photo.path)
    except Exception, error:
        print error


class ModelsChanges(models.Model):
    model_name = models.CharField(max_length=100, verbose_name=_('Model name'))
    obj_id = models.IntegerField(verbose_name='Object ID')
    operation = models.IntegerField(choices=OPERATION_CHOICES, default=0)
    operation_date = models.DateTimeField(auto_now=True)


def log_model_changes(sender, instance, **kwargs):
    '''
    Connect post_save and post_dalete signals for logging operations
    '''
    default_operation = 0
    if 'created' in kwargs.keys():
        if not kwargs['created']:
            default_operation = 1
    else:
        default_operation = 2
    ModelsChanges.objects.create(model_name=instance._meta.db_table, obj_id=instance.id, operation=default_operation)


models.signals.post_delete.connect(drop_image, sender=Student)
models.signals.post_save.connect(log_model_changes, sender=Student)
models.signals.post_delete.connect(log_model_changes, sender=Student)
