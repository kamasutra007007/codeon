from django.conf.urls import include, url
from django.conf import settings
from django.contrib import admin

from codeon.views import registration, home

admin.autodiscover()


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', home, name='home'),

    url(r'^registration/$', registration, name='registration'),
    url(r'^login/$', 'django.contrib.auth.views.login',
        {
            'template_name': 'login.html',
            'redirect_field_name': settings.LOGIN_REDIRECT_URL,
        }, name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': settings.LOGOUT_URL}, name="logout"),

    url(r'^groups/', include('student.urls', namespace='student')),
]


if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
